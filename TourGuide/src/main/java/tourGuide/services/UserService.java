package tourGuide.services;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.VisitedLocation;
import tourGuide.models.User;
import tourGuide.models.UserReward;
import tourGuide.repository.InternalUsers;

@Service
public class UserService {
	
	private final GpsUtil gpsUtil;
	private final RewardsService rewardsService;
	private Map<String, User> internalUserMap;
	
	public UserService(GpsUtil gpsUtil, RewardsService rewardsService) {
		this.gpsUtil = gpsUtil;
		this.rewardsService = rewardsService;
		InternalUsers internalUsers = new InternalUsers();
		this.internalUserMap = internalUsers.getInternalUserMap();
	}

	public User getUser(String userName) {
		return internalUserMap.get(userName);
	}
	
	public List<User> getAllUsers() {
		return internalUserMap.values().stream().collect(Collectors.toList());
	}
	
	public void addUser(User user) {
		if(!internalUserMap.containsKey(user.getUserName())) {
			internalUserMap.put(user.getUserName(), user);
		}
	}
	
	public List<UserReward> getUserRewards(User user) {
		return user.getUserRewards();
	}
	
	public VisitedLocation getUserLocation(User user) {
		VisitedLocation visitedLocation = (user.getVisitedLocations().size() > 0) ?
			user.getLastVisitedLocation() :
			trackUserLocation(user);
		return visitedLocation;
	}
	
	public VisitedLocation trackUserLocation(User user) {
		VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
		user.addToVisitedLocations(visitedLocation);
		rewardsService.calculateRewards(user);
		return visitedLocation;
	}

	public Map<String, User> getInternalUserMap() {
		return internalUserMap;
	}

	public void setInternalUserMap(Map<String, User> internalUserMap) {
		this.internalUserMap = internalUserMap;
	}

	
	
}
